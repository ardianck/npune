defmodule ApiWeb.Resolvers.JobApplications do

  require Logger

  import Ecto.Query, warn: false
  import Ecto.Changeset 

  alias Api.Repo
  alias Api.JobApplications
  alias Api.JobApplications.JobApplication
  alias Api.JobApplications.JobApplicationCategory
  alias Api.JobApplications.JobApplicationCategories

  alias Api.Documents
  alias Api.DocumentUploader
  alias Api.Documents.Document

  @doc """
    Get all job applications
  """
  def get_all_job_applications(_, params , _) do
    job_applications = JobApplication
       |> JobApplications.search(params[:keyword], params[:jcat], params[:jcity])
       |> Repo.paginate(params[:offset])
       |> Repo.all()
   {:ok, job_applications}
  end

  @doc """
    Get all job applications
  """
  def get_all_job_applications_without_drafts(_, params , _) do
    job_applications = JobApplication
       |> JobApplications.search_without_drafts(params[:keyword], params[:jcat], params[:jcity])
       |> Repo.paginate(params[:offset])
       |> Repo.all()
    {:ok, job_applications}
  end

  @doc """
  Get all job applications by category
  """
  def get_all_job_applications_by_category(_, params, _) do
    job_applications = JobApplication
      |> JobApplications.get_by_category(params[:name], params[:keyword])
      |> Repo.paginate(params[:offset])
      |> Repo.all()

    {:ok, job_applications}
  end

  @doc """
  Get all job applications by city
  """
  def get_all_job_applications_by_city(_, params, _) do
    job_applications = JobApplication
       |> JobApplications.get_by_city(params[:name], params[:keyword])
       |> Repo.paginate(params[:offset])
       |> Repo.all()

    {:ok, job_applications}
  end

  @doc """
  Get all job applications by city
  """
  def get_all_job_applications_by_company(_, params, _) do
    {company_id, ""} = Integer.parse(params[:id])
    job_applications = JobApplication
       |> JobApplications.get_by_company(company_id, params[:keyword])
       |> Repo.paginate(params[:offset])
       |> Repo.all()

    {:ok, job_applications}
  end
#  @doc """
#    Get all applications filtered
#  """
#  def get_all_job_applications_filtered(_, %{
#    offset: offset,
#    keyword: keyword,
#    time: time,
#    category: category,
#    city: city
#  }, _) do
#    job_applications = JobApplication
#      |> JobApplications.search(keyword)
#      |> Repo.paginate(50)
#      |> Repo.all()
#    {:ok, job_applications}
#  end

  @doc """
    Get a single job application
  """
  def get_by_slug(_, %{ slug: slug }, _) do
    job_application = JobApplication
      |> JobApplications.search_by_slug(slug)
      |> Repo.all();
    {:ok, job_application}
  end

  @doc """
    Get a single job application
  """
  def get_by_slug_new(_, %{ slug: slug }, _) do
    case Repo.get_by!(JobApplication, slug: slug) do
      job_application ->
        {:ok, job_application}
    end
  end

  @doc """
    Get a single job application
  """
  def get_by_id(_, %{ id: id }, _) do
    case Repo.get_by!(JobApplication, id: id) do
      job_application ->
        {:ok, job_application}
    end
  end

  @doc """
    Add new job application
  """
  def add_new_job_application(_, %{ input: params, documents: documents }, _) do
    with {:ok, %JobApplication{} = job_application} <- JobApplications.create_job_application(params) do
      # add relation with company & company
      for category <- params.categories do
        Repo.insert!(JobApplicationCategory.changeset(%JobApplicationCategory{}, %{ job_application_id: job_application.id, category_id: category }))
      end

      # insert documents related to job application (if any)
      for document <- documents do
        {:ok, document_path } = DocumentUploader.store({document, job_application})

        Repo.insert!(Document.changeset(%Document{}, %{file_type: document.content_type, path: document_path, job_application_id: job_application.id}))
      end
      # add documents
      {:ok, job_application}
    end
  end

  @doc """
    Add new job application
  """
  def update_job_application(_, %{ input: params, documents: documents }, _) do
    original_application = JobApplication |> Repo.get!(params.id)
    with {:ok, %JobApplication{} = job_application} <- JobApplications.update_job_application(original_application, params) do
      # add relation with company & company
      JobApplicationCategory
      |> JobApplicationCategories.delete_job_categories(params.id)

      for category <- params.categories do
        Repo.insert!(JobApplicationCategory.changeset(%JobApplicationCategory{}, %{ job_application_id: params.id, category_id: category }))
      end

      # insert documents related to job application (if any)
      for document <- documents do
        {:ok, document_path } = DocumentUploader.store({document, original_application})

        Repo.insert!(Document.changeset(%Document{}, %{file_type: document.content_type, path: document_path, job_application_id: params.id}))
      end

      {:ok, job_application}
    end
  end

  def update_job_application_status(_, %{ job_application_id: job_application_id, status_id: status_id }, _) do
    # found_application = JobApplication |> Repo.get!(job_application_id)
    {:ok, status } = Ecto.Type.cast(:integer, status_id)
    if job_application = JobApplication |> Repo.get!(job_application_id) do
      {:ok, updated_job_application } = job_application 
                    |> change(%{ status: status })
                    |> Repo.update()
      {:ok, updated_job_application}
    else
      res = %{code: 401, message: 'Something went wrong!'}
      {:error, res }
    end           
    # with {:ok, %JobApplication{} = job_application } <- JobApplications.update_job_application(found_application, %{ status: status_id }) do
    #   {:ok, job_application}
    # end
  end

  @doc """
    Delete a job application
  """
  def delete_job_application(_, %{ job_application_id: job_application_id }, _) do
    job_application = JobApplication
              |> Repo.get!(job_application_id)

    Repo.delete_all(JobApplicationCategory)

    if job_application |> JobApplications.delete_job_application() do
      res = %{code: 200, message: 'Job application deleted successfully!'}
      {:ok, res }
    else
      res = %{code: 401, message: 'Something went wrong!'}
      {:error, res }
    end
  end

  @doc"""
    Delete job application document
  """
  def delete_job_application_document(_, %{ job_application_id: job_application_id, path: path }, _) do
    {:ok, deleted } = Document |> Documents.delete_single_job_document(job_application_id, path)

    if deleted do
      filepath = "uploads/documents/#{job_application_id}/#{path}"
      File.rm(filepath)
      res = %{ code: 200, message: 'Document deleted successfully!'}
      {:ok, res}
    else
      res = %{ code: 401, message: 'Something went wrong!' }
      {:error, res}
    end
  end
end
